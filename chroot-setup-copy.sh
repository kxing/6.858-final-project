#!/bin/sh -x
if id | grep -qv uid=0; then
    echo "Must run setup as root"
    exit 1
fi

create_socket_dir() {
    local dirname="$1"
    local ownergroup="$2"
    local perms="$3"

    mkdir -p $dirname
    chown $ownergroup $dirname
    chmod $perms $dirname
}

set_perms() {
    local ownergroup="$1"
    local perms="$2"
    local pn="$3"

    chown $ownergroup $pn
    chmod $perms $pn
}

rm -rf /jail
mkdir -p /jail
cp -p index.html /jail

./chroot-copy.sh zookd /jail
./chroot-copy.sh zookfs /jail
#./chroot-copy.sh zoobar/pysecure/pysecure_init.py /jail

./chroot-copy.sh /bin/bash /jail

./chroot-copy.sh /usr/bin/env /jail
./chroot-copy.sh /usr/bin/python /jail
./chroot-copy.sh /bin/chown /jail
./chroot-copy.sh /bin/chmod /jail
./chroot-copy.sh /bin/local /jail

# to bring in the crypto libraries
./chroot-copy.sh /usr/bin/openssl /jail

mkdir -p /jail/usr/lib /jail/usr/lib/i386-linux-gnu /jail/lib /jail/lib/i386-linux-gnu
cp -r /usr/lib/python2.7 /jail/usr/lib
cp -r /usr/lib/pymodules /jail/usr/lib
cp /usr/lib/i386-linux-gnu/libsqlite3.so.0 /jail/usr/lib/i386-linux-gnu
cp /usr/lib/libxslt.so.1 /jail/usr/lib
cp /usr/lib/libexslt.so.0 /jail/usr/lib
cp /usr/lib/libxml2.so.2 /jail/usr/lib
cp /lib/libgcrypt.so.11 /jail/lib
cp /lib/libgpg-error.so.0 /jail/lib
cp /lib/i386-linux-gnu/libnss_dns.so.2 /jail/lib/i386-linux-gnu
cp /lib/i386-linux-gnu/libresolv.so.2 /jail/lib/i386-linux-gnu
cp -r /lib/resolvconf /jail/lib

mkdir -p /jail/usr/local/lib
cp -r /usr/local/lib/python2.7 /jail/usr/local/lib

mkdir -p /jail/etc
cp /etc/localtime /jail/etc/
cp /etc/timezone /jail/etc/
cp /etc/resolv.conf /jail/etc/

mkdir -p /jail/usr/share/zoneinfo
cp -r /usr/share/zoneinfo/America /jail/usr/share/zoneinfo/

mkdir -p /jail/tmp
chmod a+rwxt /jail/tmp

mkdir -p /jail/dev
mknod /jail/dev/urandom c 1 9

cp -r zoobar /jail/
rm -rf /jail/zoobar/db


python /jail/zoobar/zoodb.py init-person
python /jail/zoobar/zoodb.py init-profile
python /jail/zoobar/zoodb.py init-credential
python /jail/zoobar/zoodb.py init-bank
python /jail/zoobar/zoodb.py init-transfer


# Give dynamic_svc permission to run /zoobar/index.cgi
chown 685822:6858 /jail/zoobar/index.cgi

# Give dynamic_svc permissions for /zoobar/db/person
chown -R 68582:6858 /jail/zoobar/db/person/
# Remove permissions for db files for other users.
chmod 775 /jail/zoobar/db/person/
#chmod 660 /jail/zoobar/db/person/person.db



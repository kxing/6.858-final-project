#!python

# Prints out the number of zoobars that have been exchanged between the
# profile's owner and the user (ie. sent and recieved).

global api
selfuser = api.call('get_self')
visitor = api.call('get_visitor')

given = 0
received = 0

for xfer in api.call('get_xfers', username=selfuser):
    if xfer['sender'] == visitor and xfer['recipient'] == selfuser:
        given += int(xfer['amount'])
    elif xfer['sender'] == selfuser and xfer['recipient'] == visitor:
        received += int(xfer['amount'])

print 'Between %s and you, you have given %d zoobars and received %d zoobars.' % \
      (selfuser, given, received)

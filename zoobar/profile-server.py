#!/usr/bin/python

import rpclib
import sys
import os
import sandboxlib
import urllib
import socket
import stat
import bank
import bank
import profile_manager
import zoodb

from debug import *

## Cache packages that the sandboxed code might want to import
import time
import errno

class ProfileAPIServer(rpclib.RpcServer):
    def __init__(self, user, visitor, token):
        self.user = user
        self.visitor = visitor
        self.token = token
        os.setresgid(1111, 1111, 1111)
        os.setresuid(68587, 68587, 68587)

    def rpc_get_self(self):
        return self.user

    def rpc_get_visitor(self):
        return self.visitor

    def rpc_get_xfers(self, username):
        xfers = []
        for xfer in bank.get_log(username):
            xfers.append({ 'sender': xfer.sender,
                           'recipient': xfer.recipient,
                           'amount': xfer.amount,
                           'time': xfer.time,
                         })
        return xfers

    def rpc_get_user_info(self, username):
        person_db = zoodb.person_setup()
        p = person_db.query(zoodb.Person).get(username)
        if not p:
            return None
        return { 'username': p.username,
                 'profile': profile_manager.get_profile(username),
                 'zoobars': bank.balance(username),
               }

    def rpc_xfer(self, target, zoobars):
        bank.transfer(self.user, target, zoobars, self.token)

def run_profile(pcode, profile_api_client):
    globals = {'api': profile_api_client}
    exec pcode in globals

class ProfileServer(rpclib.RpcServer):
    def rpc_run(self, user, visitor):
        # Get the user's token.
        db = zoodb.credential_setup()
        credential = db.query(zoodb.Credential).get(user)
        if not credential:
            return None
        token = credential.token

        # Fetch the profile code.
        pcode = profile_manager.get_profile(user).encode('ascii', 'ignore')
        pcode = pcode.replace('\r\n', '\n')

        uid = 68586
        gid = 6858

        # We use urllib.quote to escape problematic charactes such as '/'.
        userdir = '/tmp/%s' % urllib.quote(user, '')
        if not os.path.exists(userdir):
            os.mkdir(userdir)
            os.chown(userdir, uid, 6858)
            os.chmod(userdir, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)

        (sa, sb) = socket.socketpair(socket.AF_UNIX, socket.SOCK_STREAM, 0)
        pid = os.fork()
        if pid == 0:
            if os.fork() <= 0:
                sa.close()
                ProfileAPIServer(user, visitor, token).run_sock(sb)
                sys.exit(0)
            else:
                sys.exit(0)
        sb.close()
        os.waitpid(pid, 0)

        sandbox = sandboxlib.Sandbox(userdir, uid, '/pysecure_socks/profilesvc/lockfile')
        with rpclib.RpcClient(sa) as profile_api_client:
            return sandbox.run(lambda: run_profile(pcode, profile_api_client))

(_, dummy_zookld_fd, sockpath) = sys.argv

s = ProfileServer()
s.run_sockpath_fork(sockpath)

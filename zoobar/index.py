from flask import g, render_template, request
from login import requirelogin
from debug import *
from zoodb import *

import profile_manager

@catch_err
@requirelogin
def index():
    if 'profile_update' in request.form:
        profile_manager.set_profile(g.user.person.username, \
                                    request.form['profile_update'], \
                                    g.user.token)

        ## also update the cached version (see login.py)
        g.user.person.profile = profile_manager.get_profile(g.user.person.username)
    return render_template('index.html')

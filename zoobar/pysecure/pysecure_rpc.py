import rpclib

def socket_name(svc_name):
    return '/pysecure_socks/%s/sock' % svc_name

def make_rpc_decorator():
    rpc_list = {}

    # Decorator with arguments calls return value with function as argument
    def rpc(run_as):
        def inner(func):
            # Return value of this will replace the decorated function
            if run_as not in rpc_list:
                rpc_list[run_as] = {}
            rpc_list[run_as][func.__name__] = func
            def wrapped_rpc(*args, **kwargs):
                # print 'Replacement RPC function %s with args %s %s to rpc server %s' % (func.__name__, args, kwargs, run_as)
                # Send function call request to RPC service
                with rpclib.client_connect(socket_name(run_as)) as conn:
                    return conn.call(func.__name__, args, kwargs)
            return wrapped_rpc
        return inner

    # Will be used by RPC service to look up functions
    rpc.rpc_list = rpc_list
    return rpc

rpc = make_rpc_decorator()

def start_service(svc_name):
    s = rpclib.RpcServer()
    rpc_list = rpc.rpc_list
    if svc_name not in rpc_list:
        print 'Error: No RPCs found for service %s. Terminating service.' % svc_name
        return
    s.rpc_list = rpc_list[svc_name]
    s.run_sockpath_fork(socket_name(svc_name))

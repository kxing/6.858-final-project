import os
import sys
from pysecure_resource import PySecureResource
from pysecure_config import SOCKETS_DIR_PATH

class PySecureService:
  def __init__(self, service_name, directory, command, owner_resources=[], bundle=None, runs_with_root=False):
    self.service_name = service_name
    self.uid = None
    self.gid = None
    self.directory = directory
    self.command = command
    self.owner_resources = owner_resources
    self.shared_bundle = bundle
    self.runs_with_root = runs_with_root
    self.sock_path = os.path.join(SOCKETS_DIR_PATH, self.service_name, 'sock')

  def get_service_name(self):
    return self.service_name

  def get_owner_resources(self):
    return self.owner_resources

  def get_shared_bundle(self):
    return self.shared_bundle

  def needs_root(self):
    return self.runs_with_root

  def set_uid(self, uid):
    self.uid = uid

  def set_gid(self, gid):
    self.gid = gid

  def launch(self):
    pid = os.fork()
    # TODO(kxing): Do bookkeeping with service_name and sockets so that RPCs work correctly.
    if pid == 0:
      os.chroot(self.directory)
      os.setresgid(self.gid, self.gid, self.gid)
      os.setresuid(self.uid, self.uid, self.uid)

      # TODO(kxing): Get rid of the dummy 0 parameter.
      shell_command = [self.command, '0']
      shell_command.append(self.sock_path)
      os.execv(self.command, shell_command)
      sys.exit(0)


  # TODO: owner of socket directory is same as service, problem for profilesvc that is run as root for sandboxing stuff...
  # returns socket directory as a resource
  def create_socket_dir(self, sockets_dir_path):
    dir_path = os.path.join(sockets_dir_path, self.service_name)
    if not os.path.exists(dir_path):
      os.mkdir(dir_path)
    resource = PySecureResource(dir_path, "rwx", "rx", "rx")
    return resource

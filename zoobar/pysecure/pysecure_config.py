# End-user defined config file for pysecure library

# Jail directory
JAIL_DIRECTORY = 'jail'

# Sockets directory path inside jail
SOCKETS_DIR_PATH = '/pysecure_socks/'

class PySecureConfig:
  def __init__(self, app_name, uid_range, gid_range, \
               dynamic_svc_uid=None, dynamic_svc_gid=None):
    self.app_name = app_name
    self.uid_range = uid_range
    self.gid_range = gid_range
    self.resources = []
    self.bundles = []
    self.services = []
    self.dynamic_svc_uid = dynamic_svc_uid
    self.dynamic_svc_gid = dynamic_svc_gid

  def register_resources(self, resources):
    self.resources = resources

  def register_bundles(self, bundles):
    self.bundles = bundles

  def register_services(self, services):
    self.services = services

  def get_resources(self):
    return self.resources

  def get_bundles(self):
    return self.bundles

  def get_services(self):
    return self.services

  def get_app_name(self):
    return self.app_name

  def get_uid_range(self):
    return self.uid_range

  def get_gid_range(self):
    return self.gid_range

  def get_dynamic_svc_uid(self):
    return self.dynamic_svc_uid

  def get_dynamic_svc_gid(self):
    return self.dynamic_svc_gid

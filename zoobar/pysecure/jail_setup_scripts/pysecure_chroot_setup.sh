#!/bin/bash
if id | grep -qv uid=0; then
    echo "Must run setup as root"
    exit 1
fi

SCRIPT_PATH=$(cd `dirname ${0}`; pwd);
FILEPATH="$SCRIPT_PATH/jail_resources.txt"
CHROOT_COPY_CMD="$SCRIPT_PATH/chroot-copy.sh"

copy_file_structure(){
  local line="$1"
  local jail="$2"

  #echo "Inside copy_file_structure"
  D=`dirname $line`
  # copy directory structure for anything in /bin, /usr, /lib, /etc
  BASE=`echo $line | awk -F"/" '{print $2}'`
  #echo "BASE: $BASE"
  if [[ $BASE == "bin" ]] || [[ $BASE == "usr" ]] || [[ $BASE == "lib" ]] || [[ $BASE == "etc" ]]; then
    mkdir -p /jail/$D
    cp -rL $line /$jail$line
  else
    cp -rL $line /$jail
  fi
}

while read line
do
  echo "Copying Resource and Dependencies: $line"
  if [ -f "$line" ]; then
    #echo "IS A FILE"
    if [ -x "$line" ]; then
      $CHROOT_COPY_CMD $line /jail
      cp $line /jail

    elif [ -h "$line" ]; then
      copy_file_structure "$line" jail
    else
      copy_file_structure "$line" jail
    fi

  else
    copy_file_structure "$line" jail
  fi
done < $FILEPATH

echo "Finishing jail setup..."
mkdir -p /jail/tmp
chmod a+rwxt /jail/tmp
mkdir -p /jail/dev
mknod /jail/dev/urandom c 1 9


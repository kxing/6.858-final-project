#!/bin/sh -x
if id | grep -qv uid=0; then
    echo "Must run setup as root"
    exit 1
fi

rm -fr /jail
mkdir /jail

./zoobar/pysecure/jail_setup_scripts/pysecure_chroot_setup.sh

./zoobar/pysecure/jail_setup_scripts/pysecure_initialize_resources.sh zoobar


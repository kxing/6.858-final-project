#!/bin/sh -x

# Defined by Developer
# Creates resources for jail

APP_NAME="$1"

set_perms() {
    local ownergroup="$1"
    local perms="$2"
    local pn="$3"

    chown $ownergroup $pn
    chmod $perms $pn
}

echo 'Start creating sqlite db files'
python /jail/$APP_NAME/zoodb.py init-person
python /jail/$APP_NAME/zoodb.py init-profile
python /jail/$APP_NAME/zoodb.py init-credential
python /jail/$APP_NAME/zoodb.py init-bank
python /jail/$APP_NAME/zoodb.py init-transfer

echo 'Finished creating database files'

# Give dynamic_svc permission to run /zoobar/index.cgi
chown 685822:6858 /jail/zoobar/index.cgi


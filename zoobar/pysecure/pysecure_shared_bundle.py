class PySecureSharedBundle:
  next_bundle_id = 0

  def __init__(self, resources, used_by_dynamic_svc=False):
    self.resources = resources
    self.bundle_id = PySecureSharedBundle.next_bundle_id
    PySecureSharedBundle.next_bundle_id += 1
    self.gid = None
    self.used_by_dynamic_svc = used_by_dynamic_svc

  def get_bundle_id(self):
    return self.bundle_id

  def get_resources(self):
    return self.resources

  def set_gid(self, gid):
    self.gid = gid

  def needed_by_dynamic_svc(self):
    return self.used_by_dynamic_svc


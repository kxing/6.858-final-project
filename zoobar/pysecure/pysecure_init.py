#!/usr/bin/python
from pysecure import PySecure, PySecureConfig, PySecureResource, PySecureService, PySecureSharedBundle

person_dir_resource = \
  PySecureResource('/zoobar/db/person/',              'rwx', 'rwx', '')
person_db_resource = \
  PySecureResource('/zoobar/db/person/person.db',     'rw',  'rw',  '')
cred_dir_resource = \
  PySecureResource('/zoobar/db/cred/',                'rwx', '',    '')
cred_db_resource = \
  PySecureResource('/zoobar/db/cred/cred.db',         'rw',  '',    '')
bank_dir_resource = \
  PySecureResource('/zoobar/db/bank/',                'rwx', '',    '')
bank_db_resource = \
  PySecureResource('/zoobar/db/bank/bank.db',         'rw',  '',    '')
transfer_dir_resource = \
  PySecureResource('/zoobar/db/transfer/',            'rwx', 'rx',  '')
transfer_db_resource = \
  PySecureResource('/zoobar/db/transfer/transfer.db', 'rw',  'r',   '')
profile_dir_resource = \
  PySecureResource('/zoobar/db/profile/',             'rwx', '',    '')
profile_db_resource = \
  PySecureResource('/zoobar/db/profile/profile.db',   'rw',  '',    '')

zoobar_bundle = PySecureSharedBundle([person_dir_resource, person_db_resource, transfer_dir_resource, transfer_db_resource], used_by_dynamic_svc=True)

# resources are chrooted at /jail from zook.conf

config = PySecureConfig('zoobar', (5000, 6000), (5000, 6000), dynamic_svc_gid=1111)

config.register_resources([ \
  person_dir_resource, \
  person_db_resource, \
  cred_dir_resource, \
  cred_db_resource, \
  bank_dir_resource, \
  bank_db_resource, \
  transfer_dir_resource, \
  transfer_db_resource, \
  profile_dir_resource, \
  profile_db_resource, \
  ])

config.register_bundles([zoobar_bundle])

config.register_services([ \
  PySecureService('echosvc', '/', '/zoobar/echo-server.py',[]) , \
  PySecureService('authsvc', '/', '/zoobar/auth.py', [cred_dir_resource, cred_db_resource], zoobar_bundle), \
  PySecureService('banksvc', '/', '/zoobar/bank.py', [bank_dir_resource, bank_db_resource, transfer_dir_resource, transfer_db_resource]), \
  PySecureService('profilesvc', '/', '/zoobar/profile-server.py', [], runs_with_root=True), \
  PySecureService('profilemanagersvc', '/', '/zoobar/profile_manager.py', [profile_dir_resource, profile_db_resource]) \
  ])


PySecure.run_with_config(config)

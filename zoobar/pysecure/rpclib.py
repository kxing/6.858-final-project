import os
import sys
import socket
import stat
import errno
import json
import exceptions

def parse_req(req):
    # The JSON is a list: [method, args, kwargs]
    return tuple(json.loads(req))

def format_req(method, args, kwargs):
    return json.dumps([method, args, kwargs])

def parse_resp(resp):
    return json.loads(resp)

def format_resp(resp):
    return json.dumps(resp)

def buffered_readlines(sock):
    buf = ''
    while True:
        while '\n' in buf:
            (line, nl, buf) = buf.partition('\n')
            yield line
        try:
            newdata = sock.recv(4096)
            if newdata == '':
                break
            buf += newdata
        except IOError, e:
            if e.errno == errno.ECONNRESET:
                break

class RpcServer(object):
    def run_sock(self, sock):
        lines = buffered_readlines(sock)
        for req in lines:
            (method, args, kwargs) = parse_req(req)
            m = self.rpc_list[method]
            try:
                ret = (None, m(*args, **kwargs))
            except BaseException as e:
                exception_name = e.__class__.__name__
                args = e.args
                if exception_name not in exceptions.__dict__:
                    # Convert user-defined exception to built-in exception
                    args = (e.message, )
                    if exception_name.endswith('Error'):
                        exception_name = 'RuntimeError'
                    elif exception_name.endswith('Warning'):
                        exception_name = 'Warning'
                    else:
                        exception_name = 'Exception'
                ret = (exception_name, args)
            sock.sendall(format_resp(ret) + '\n')

    def run_sockpath_fork(self, sockpath):
        if os.path.exists(sockpath):
            s = os.stat(sockpath)
            if not stat.S_ISSOCK(s.st_mode):
                raise Exception('%s exists and is not a socket' % sockpath)
            os.unlink(sockpath)

        server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        server.bind(sockpath)

        # Allow anyone to connect.
        # For access control, use directory permissions.
        os.chmod(sockpath, 0777)

        server.listen(5)
        while True:
            conn, addr = server.accept()
            pid = os.fork()
            if pid == 0:
                # fork again to avoid zombies
                if os.fork() <= 0:
                    self.run_sock(conn)
                    sys.exit(0)
                else:
                    sys.exit(0)
            conn.close()
            os.waitpid(pid, 0)

class RpcClient(object):
    def __init__(self, sock):
        self.sock = sock
        self.lines = buffered_readlines(sock)

    def call(self, method, args, kwargs):
        self.sock.sendall(format_req(method, args, kwargs) + '\n')
        exception_name, data = parse_resp(self.lines.next())
        if exception_name is None:
            return data
        rpc_exception = exceptions.__dict__[exception_name](*data)
        raise rpc_exception

    def close(self):
        self.sock.close()

    ## __enter__ and __exit__ make it possible to use RpcClient()
    ## in a "with" statement, so that it's automatically closed.
    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

def client_connect(pathname):
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.connect(pathname)
    return RpcClient(sock)


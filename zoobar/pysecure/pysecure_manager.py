import os
import subprocess
import pysecure_config
from pysecure_config import SOCKETS_DIR_PATH
from pysecure_config import JAIL_DIRECTORY

# Singleton class
# Assigns uid and gids to services based on user input ranges
# Manages resources and permissions for all services
# TODO(qlong) not done yet
class PySecureManager:

  def __init__(self):
    self.uid_start = None
    self.uid_end = None
    self.gid_start = None
    self.gid_end = None
    self.app_name = None
    self.dynamic_svc_uid = None
    self.dynamic_svc_gid = None

    # Maps service names to services
    self.services = {}
    # Maps service names to UID
    # { 'auth_svc': 68584, 'bank_svc': 68585}
    self.service_uids = {}
    # Maps service names to GID
    self.service_gids = {}

    # Maps resource paths to resources.
    self.resources = {}
    # Maps resource paths to owner UIDs.
    self.resource_uids = {}
    # Maps resource paths to GIDs.
    self.resource_gids = {}

    # Maps bundle IDs to bundles.
    self.bundles = {}
    # Maps bundle IDs to GIDs.
    self.bundle_gids = {}

    # State needed for UID and GID allocation.
    self.next_uid = None
    self.next_gid = None

  def get_uid(self, service_name):
    return self.service_uids[service_name]

  def get_gid(self, service_name):
    return self.service_gids[service_name]

  def _register_resources(self, resources):
    for resource in resources:
      resource_path = resource.get_resource_path()
      self.resources[resource_path] = resource

  def _register_bundles(self, bundles):
    for bundle in bundles:
      bundle_id = bundle.get_bundle_id()
      self.bundles[bundle_id] = bundle

  def _register_services(self, services):
    for service in services:
      service_name = service.get_service_name()
      self.services[service_name] = service

  def _parse_config(self, config):
    self._register_resources(config.get_resources())
    self._register_bundles(config.get_bundles())
    self._register_services(config.get_services())

    self.app_name = config.get_app_name()
    self.uid_start, self.uid_end = config.get_uid_range()
    self.gid_start, self.gid_end = config.get_gid_range()
    self.next_uid = self.uid_start
    self.next_gid = self.gid_start

    self.dynamic_svc_uid = config.get_dynamic_svc_uid()
    self.dynamic_svc_gid = config.get_dynamic_svc_gid()

  def _get_next_uid(self):
    current_uid = self.next_uid
    if current_uid > self.uid_end:
      raise Error('Ran out of UIDs. Please allocate more UIDs for PySecure.')
    self.next_uid += 1
    return current_uid

  def _get_next_gid(self):
    current_gid = self.next_gid
    self.next_gid += 1
    if current_gid > self.gid_end:
      raise Error('Ran out of GIDs. Please allocate more GIDs for PySecure.')
    return current_gid

  def _get_dynamic_svc_uid(self):
    if self.dynamic_svc_uid is None:
      raise Error('Dynamic service UID is not set.')
    return self.dynamic_svc_uid

  def _get_dynamic_svc_gid(self):
    if self.dynamic_svc_gid is None:
      raise Error('Dynamic service GID is not set.')
    return self.dynamic_svc_gid

  def _assign_uids(self):
    # Assign UIDs to all of the services.
    for service in self.services.values():
      service_name = service.get_service_name()

      uid = None
      if service.needs_root():
        uid = 0
      else:
        uid = self._get_next_uid()

      self.service_uids[service_name] = uid
      service.set_uid(uid)

      for resource in service.get_owner_resources():
        if resource.is_owned_by_dynamic_svc():
          raise Error('Resource %s owned by the dynamic service and a different service.' % resource_path)
        resource_path = resource.get_resource_path()
        if resource_path in self.resource_uids:
          raise Error('Resource %s owned by more than one service.' % resource_path)
        self.resource_uids[resource_path] = uid
        resource.set_uid(uid)

    # Assign owner UIDs for the remaining resources.
    for resource_path, resource in self.resources.iteritems():
      if resource_path not in self.resource_uids:
        uid = None
        if resource.is_owned_by_dynamic_svc():
          uid = self._get_dynamic_svc_uid()
        else:
          uid = self._get_next_uid()
        self.resource_uids[resource_path] = uid
        resource.set_uid(uid)

  def _assign_gids(self):
    dynamic_svc_bundle_allocated = False

    for bundle_id, bundle in self.bundles.iteritems():
      gid = None
      if bundle.needed_by_dynamic_svc():
        gid = self._get_dynamic_svc_gid()
        if dynamic_svc_bundle_allocated:
          raise Error('Only one bundle allowed for the dynamic service.')
        dynamic_svc_bundle_allocated = True
      else:
        gid = self._get_next_gid()

      self.bundle_gids[bundle_id] = gid
      bundle.set_gid(gid)
      for resource in bundle.get_resources():
        resource_path = resource.get_resource_path()

        if resource_path in self.resource_gids:
          raise Error('Resource %s owned by more than one shared bundle.' % resource_path)
        self.resource_gids[resource_path] = gid
        resource.set_gid(gid)

    # Assign GIDs to the remaining services.
    for service in self.services.values():
      service_name = service.get_service_name()

      gid = None

      if service.get_shared_bundle():
        bundle_id = service.get_shared_bundle().get_bundle_id()
        gid = self.bundle_gids[bundle_id]
      else:
        gid = self._get_next_gid()

      self.service_gids[service_name] = gid
      service.set_gid(gid)

    # Assign GIDs to the leftover resources.
    for resource_path, resource in self.resources.iteritems():
      if resource_path not in self.resource_gids:
        gid = self._get_next_gid()
        self.resource_gids[resource_path] = gid
        resource.set_gid(gid)

  # TODO(qlong)
  # set up database and correct permissions
  # set up correct resource permissions
  def _setup_jail(self):
    print "SETTING UP JAIL PERMISSIONS"
    for resource in self.resources.values():
      resource.set_permissions()

  # owner of socket directory is same as service, problem for profilesvc that is run as root?
  def _setup_socket_dirs(self, services):
    print "SETTING UP SOCKET DIRS"
    if not os.path.exists(SOCKETS_DIR_PATH):
      os.mkdir(SOCKETS_DIR_PATH)
    for service in services:
      # TODO add resource to master list?
      socket_dir_resource = service.create_socket_dir(SOCKETS_DIR_PATH)
      uid = self.service_uids[service.get_service_name()]
      gid = self.service_gids[service.get_service_name()]
      socket_dir_resource.set_uid(uid)
      socket_dir_resource.set_gid(gid)
      socket_dir_resource.set_permissions()

  def _run_services(self, services):
    print "RUNNING SERVICES"
    for service in services:
      service.launch()

  def run_with_config(self, config):
    self._parse_config(config)

    self._assign_uids()
    self._assign_gids()

    self._setup_jail()
    self._setup_socket_dirs(self.services.values())
    self._run_services(self.services.values())

PySecure = PySecureManager()


import os
import stat

class PySecureResource:

  def __init__(self, resource_path, owner_perm = "", group_perm = "", other_perm = "", \
               owned_by_dynamic_svc=False):
    self.resource = resource_path
    self.uid = None
    self.gid = None
    self.owner_perm = owner_perm # 'r', 'rw', or 'rwx'
    self.group_perm = group_perm
    self.other_perm = other_perm
    self.owned_by_dynamic_svc = owned_by_dynamic_svc

  def get_resource_path(self):
    return self.resource

  def is_owned_by_dynamic_svc(self):
    return self.owned_by_dynamic_svc

  def set_uid(self, uid):
    self.uid = uid

  def set_gid(self, gid):
    self.gid = gid

  def set_permissions(self):
    os.chown(self.resource, self.uid, self.gid)

    # resource is a directory, do recursive chown
    if os.path.isdir(self.resource):
      self.recursive_chown()

    os.chmod(self.resource, self.get_perm_mode())

  def get_perm_mode(self):
    mode = 0

    if 'r' in self.owner_perm:
      mode = mode | stat.S_IRUSR
    if 'w' in self.owner_perm:
      mode = mode | stat.S_IWUSR
    if 'x' in self.owner_perm:
      mode = mode | stat.S_IXUSR

    if 'r' in self.group_perm:
      mode = mode | stat.S_IRGRP
    if 'w' in self.group_perm:
      mode = mode | stat.S_IWGRP
    if 'x' in self.group_perm:
      mode = mode | stat.S_IXGRP

    if 'r' in self.other_perm:
      mode = mode | stat.S_IROTH
    if 'w' in self.other_perm:
      mode = mode | stat.S_IWOTH
    if 'x' in self.other_perm:
      mode = mode | stat.S_IXOTH

    return mode

  def recursive_chown(self):
    # equivalent to chown -R uid:gid path 
    for root, dirs, files in os.walk(self.resource):
      for d in dirs:
        os.chown(os.path.join(root, d), self.uid, self.gid)
      for f in files:
        os.chown(os.path.join(root, f), self.uid, self.gid)


#!/usr/bin/python

from zoodb import *
import auth
import sys
from debug import *

from pysecure.pysecure_rpc import rpc, start_service

@rpc(run_as='profilemanagersvc')
def get_profile(username):
    db = profile_setup()
    profile = db.query(Profile).get(username)
    if not profile:
        return None
    return profile.profile

@rpc(run_as='profilemanagersvc')
def set_profile(username, profile, token):
    # Do token checking.
    if not auth.check_token(username, token):
        return False

    # Update the profile.
    db = profile_setup()
    profile_entry = db.query(Profile).get(username)
    if not profile:
        return False
    profile_entry.profile = profile
    db.commit()
    return True

@rpc(run_as='profilemanagersvc')
def register(username):
    db = profile_setup()
    profile = db.query(Profile).get(username)
    if profile:
        return None
    new_profile = Profile()
    new_profile.username = username
    db.add(new_profile)
    db.commit()
    return True

if __name__ == '__main__':
    start_service('profilemanagersvc')

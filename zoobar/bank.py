#!/usr/bin/python

from zoodb import *
from debug import *

import auth
import time
import traceback

from pysecure.pysecure_rpc import rpc, start_service

@rpc(run_as='banksvc')
def transfer(sender, recipient, zoobars, token):
    try:
        if zoobars < 0:
            raise ValueError("Negative zoobars to transfer.")
        if sender == recipient:
            raise ValueError("Cannot transfer to self.")
        if not auth.check_token(sender, token):
            raise ValueError("Invalid credentials.")

        bankdb = bank_setup()
        senderp = bankdb.query(Bank).get(sender)
        recipientp = bankdb.query(Bank).get(recipient)

        sender_balance = senderp.zoobars - zoobars
        recipient_balance = recipientp.zoobars + zoobars

        if sender_balance < 0 or recipient_balance < 0:
            raise ValueError("Insufficient funds.")

        senderp.zoobars = sender_balance
        recipientp.zoobars = recipient_balance
        bankdb.commit()

        transfer = Transfer()
        transfer.sender = sender
        transfer.recipient = recipient
        transfer.amount = zoobars
        transfer.time = time.asctime()

        transferdb = transfer_setup()
        transferdb.add(transfer)
        transferdb.commit()
        return True
    except (KeyError, ValueError, AttributeError) as e:
        traceback.print_exc()
    return False

@rpc(run_as='banksvc')
def balance(username):
    db = bank_setup()
    account = db.query(Bank).get(username)
    return account.zoobars

@rpc(run_as='banksvc')
def initialize_account(username):
    db = bank_setup()
    person = db.query(Bank).get(username)
    if person:
        return False

    newaccount = Bank()
    newaccount.username = username
    db.add(newaccount)

    db.commit()

def get_log(username):
    db = transfer_setup()
    return db.query(Transfer).filter(or_(Transfer.sender==username,
                                         Transfer.recipient==username))

if __name__ == '__main__':
    start_service('banksvc')

#!/usr/bin/python

from zoodb import *
from debug import *

import bank
import hashlib
import os
import pbkdf2
import profile_manager
import random

from pysecure.pysecure_rpc import rpc, start_service

def compute_hashed_password(password, salt):
    return pbkdf2.PBKDF2(password, salt).hexread(32)

def newtoken(db, credential):
    hashinput = "%s%.10f" % (credential.password, random.random())
    credential.token = hashlib.md5(hashinput).hexdigest()
    db.commit()
    return credential.token

@rpc(run_as='authsvc')
def login(username, password):
    db = credential_setup()
    credential = db.query(Credential).get(username)
    if not credential:
        return None
    if credential.password == compute_hashed_password(password, credential.salt):
        return newtoken(db, credential)
    else:
        return None

@rpc(run_as='authsvc')
def register(username, password):
    credential_db = credential_setup()
    credential = credential_db.query(Credential).get(username)
    if credential:
        return None
    newcredential = Credential()
    newcredential.username = username
    newcredential.salt = unicode(os.urandom(8), "latin-1")
    newcredential.password = compute_hashed_password(password, newcredential.salt)
    credential_db.add(newcredential)

    person_db = person_setup()
    person = person_db.query(Person).get(username)
    if person:
        return None
    newperson = Person()
    newperson.username = username
    person_db.add(newperson)

    bank.initialize_account(username)

    credential_db.commit()
    person_db.commit()

    if not profile_manager.register(username):
        return None

    return newtoken(credential_db, newcredential)

@rpc(run_as='authsvc')
def check_token(username, token):
    db = credential_setup()
    credential = db.query(Credential).get(username)
    if credential and credential.token == token:
        return True
    else:
        return False

if __name__ == '__main__':
    start_service('authsvc')

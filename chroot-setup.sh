#!/bin/sh -x
if id | grep -qv uid=0; then
    echo "Must run setup as root"
    exit 1
fi

# run pysecure jail creation
./zoobar/pysecure/jail_setup_scripts/pysecure_create_jail.sh
